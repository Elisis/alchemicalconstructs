package elisis.alchemicalconstructs;

import elisis.alchemicalconstructs.proxy.CommonProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Main.MODID, name = Main.MODNAME, version = Main.VERSION, 
	acceptedMinecraftVersions = "1.12.2")

public class Main {
	
	public static final String MODID = "alchemicalconstructs";
	public static final String MODNAME = "Alchemical Constructs";
	public static final String VERSION = "0.0.1";
	
	
	@Mod.Instance
	public static Main instance;
	
	
	public static org.apache.logging.log4j.Logger logger;
	
	
	@SidedProxy(clientSide = "elisis.alchemicalconstructs.proxy.ClientProxy", serverSide = "elisis.alchemicalconstructs.proxy.ServerProxy")
	public static CommonProxy proxy;
	
	
	@Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        
		logger = event.getModLog();
        proxy.preInit(event);
    
	}
	
	@Mod.EventHandler
    public void init(FMLInitializationEvent e) {
        
		proxy.init(e);
    
	}
	
	@Mod.EventHandler
    public void postInit(FMLPostInitializationEvent e) {
        
		proxy.postInit(e);
    
	}

}



