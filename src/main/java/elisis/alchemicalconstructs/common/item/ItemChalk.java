package elisis.alchemicalconstructs.common.item;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemChalk extends BaseItem {
	
	//t
	
	public ItemChalk() {
		
		super("chalk", "chalk");
		
		this.setCreativeTab(CreativeTabs.TOOLS);
		this.setMaxDamage(256);
		this.setMaxStackSize(1);
	
	}
	
	
	//Messy, but it works
	@Override
	public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		
		
		IBlockState block = worldIn.getBlockState(pos);
		
		if (block.getMaterial() == Material.ROCK && !(block.getClass()
				.equals(Blocks.BEDROCK.getClass()))) {
			
			//Bring up GUI -> See Chisel
			
			worldIn.destroyBlock(pos, false);
		
		}
		
		try {Thread.sleep(500);} 
			catch (InterruptedException e) {e.printStackTrace();}
			
		return null;
		
	}

}
