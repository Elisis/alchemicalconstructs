package elisis.alchemicalconstructs.common.item;

import net.minecraft.item.Item;

public class BaseItem extends Item {
	
	public BaseItem(String registryName, String unlocalizedName) {
		
		this.setRegistryName(registryName);
		this.setUnlocalizedName(unlocalizedName);
		
	}

}
