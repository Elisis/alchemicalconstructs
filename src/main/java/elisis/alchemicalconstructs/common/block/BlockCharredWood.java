package elisis.alchemicalconstructs.common.block;

import elisis.alchemicalconstructs.Main;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockCharredWood extends Block {

	public BlockCharredWood() {
		
		super(Material.SAND);
		
		this.setUnlocalizedName(Main.MODID + ".charred_wood");
		this.setRegistryName("charred_wood");
		
	}

}
